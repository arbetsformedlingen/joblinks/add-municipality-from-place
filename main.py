import csv
import os
import sys

import ndjson
import json
# import requests

taxonomy_graphql_endpoint = 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql'


def get_manual_mappings(file_path):
    '''
    :param file_path: Filepath to mappingsfile.
    :return: Dict with key: geolabel (term in lowercase) and value: municipality taxonomyId
    '''
    mappings = {}
    with open(file_path, newline='', encoding="utf8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            geo_label = str(row[0]).lower()
            mun_taxonomy_id = row[3]
            mappings[geo_label] = mun_taxonomy_id
    return mappings


def open_ads_from_std_in():
    ads = ndjson.load(sys.stdin)
    return ads


def open_ads_from_file(rel_filepath):
    '''
    Opens input file with ads and loads ndjson to dictionary
    :param rel_filepath: For example 'resources/ads_to_process'
    :return: Ads loaded from local file
    '''
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    ads_path = currentdir + rel_filepath

    with open(ads_path, encoding="utf8") as ads_file:
        ads = ndjson.load(ads_file)
    return ads


def write_ads_to_file(ads):
    # Write ads to output file (ndjson)...
    ads_output_path = currentdir + 'resources' + os.sep + 'ads_20200514_hash_sorted_ssyk_municipality_region.json'
    with open(ads_output_path, 'w', encoding="utf8") as f:
        ndjson.dump(ads, f, ensure_ascii=False)


def print_ads(ads):
    output_json = ndjson.dumps(ads, ensure_ascii=False)
    print(output_json, file=sys.stdout)


def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


# INFO: 2020-12-02. Call to Taxonomy API commented out and replaced by reading from .json-file instead.
# def read_taxonomy_municipalities_data():
#     # Note: See https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql for help with creating graphQL-querys.
#     query = '''
#             {
#               concepts(type: "municipality") {
#                 id
#                 preferred_label
#                 broader(type: "region") {
#                   id
#                   preferred_label
#                   broader(type: "country") {
#                     id
#                     preferred_label
#                   }
#                 }
#               }
#             }
#             '''
#     data = {'query': query}
#
#     headers = {"Accept": "application/json", "api-key": taxonomy_api_key}
#     municipalities_data = []
#
#     with requests.post(taxonomy_graphql_endpoint, params=data, headers=headers) as r:
#         r.raise_for_status()
#         response = r.json()
#         data = response['data']
#         if data:
#             concepts = data['concepts']
#             if concepts:
#                 for concept in concepts:
#                     municipalities_data.append(concept)
#
#         return municipalities_data



# def read_taxonomy_countries():
#     query = '''
#             {
#               concepts(type: "country") {
#                 id
#                 preferred_label
#               }
#             }
#             '''
#     data = {'query': query}
#
#     headers = {"Accept": "application/json", "api-key": taxonomy_api_key}
#     countries = []
#
#     with requests.post(taxonomy_graphql_endpoint, params=data, headers=headers) as r:
#         r.raise_for_status()
#         response = r.json()
#         data = response['data']
#         if data:
#             concepts = data['concepts']
#             if concepts:
#                 for concept in concepts:
#                     cleaned_concept = {
#                         'id': concept['id'],
#                         'preferred_label': clean_country_label(concept['preferred_label'])
#                     }
#
#                     countries.append(cleaned_concept)
#
#         return countries

def read_taxonomy_municipalities_data_from_file():
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    taxonomyvalues_filepath = currentdir + 'resources/taxonomy_v1_municipalities_regions_countries.json'
    with open(taxonomyvalues_filepath, encoding="utf8") as tax_file:
        municipalities_data = []
        taxonomy_response = json.load(tax_file)
        data = taxonomy_response['data']
        if data:
            concepts = data['concepts']
            if concepts:
                for concept in concepts:
                    municipalities_data.append(concept)

        return municipalities_data

def clean_country_label(country_label):
    # Clean 'Åland (tillhör finland)' -> 'Åland'
    cleaned_label = country_label.split('(')[0]
    # Clean 'Mexiko/Mexikos förenta stater' -> 'Mexiko'
    cleaned_label = cleaned_label.split('/')[0]
    cleaned_label = cleaned_label.strip()

    return cleaned_label


def read_taxonomy_countries_from_file():
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    taxonomyvalues_filepath = currentdir + 'resources/taxonomy_v1_countries.json'

    countries = []

    with open(taxonomyvalues_filepath, encoding="utf8") as tax_file:

        taxonomy_response = json.load(tax_file)
        data = taxonomy_response['data']
        if data:
            concepts = data['concepts']
            if concepts:
                for concept in concepts:
                    cleaned_concept = {
                        'id': concept['id'],
                        'preferred_label': clean_country_label(concept['preferred_label'])
                    }

                    countries.append(cleaned_concept)

        return countries

def read_env_variables():

    global local_filepath
    local_filepath = os.getenv('LOCAL_FILEPATH', '')

    # INFO: 2020-12-02. Call to Taxonomy API commented out and replaced by reading from .json-file instead.
    # global taxonomy_api_key
    # taxonomy_api_key = os.getenv('TAXONOMY_API_KEY', '')
    #
    # if not taxonomy_api_key:
    #     print_error('Env variable TAXONOMY_API_KEY has no value and must be set!')
    #     sys.exit(1)

def remove_duplicates(seq):
    '''
    Removes duplicate dicts in list while preserving order.
    :param seq: List of dicts
    :return: Unique list of dicts
    '''
    seen = set()
    unique_list = []
    for d in seq:
        t = tuple(d.items())
        if t not in seen:
            seen.add(t)
            unique_list.append(d)
    return unique_list


if __name__ == '__main__':

    read_env_variables()

    global currentdir
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep

    if local_filepath:
        ads = open_ads_from_file(local_filepath)
    else:
        ads = open_ads_from_std_in()

    municipalities_path = currentdir + 'resources' + os.sep + 'municipalities_geo_to_municipality.csv'
    # print('municipalities_path: %s' % municipalities_path)
    geolabel_to_municipality_taxid = get_manual_mappings(municipalities_path)

    municipalities_taxonomy_data = read_taxonomy_municipalities_data_from_file()

    municipality_taxonomy_mappings = {}
    for municipality_entry in municipalities_taxonomy_data:
        municipality_taxonomy_mappings[municipality_entry['id']] = {
            'mun_id': municipality_entry['id'],
            'mun_preferred_label': municipality_entry['preferred_label'],
            'region_id': municipality_entry['broader'][0]['id'],
            'region_preferred_label': municipality_entry['broader'][0]['preferred_label'],
            'country_id': municipality_entry['broader'][0]['broader'][0]['id'],
            'country_preferred_label': municipality_entry['broader'][0]['broader'][0]['preferred_label']
        }

    countries_taxonomy = read_taxonomy_countries_from_file()
    country_label_to_data_mappings = {item['preferred_label'].lower():item for item in countries_taxonomy}

    for ad in ads:
        try:

            workplace_addresses = []

            enriched_geos = ad['text_enrichments_results']['enrichedbinary_result']['enriched_candidates']['geos']
            geo_labels = [enriched_geo['concept_label'].lower() for enriched_geo in enriched_geos]

            for geo_label in geo_labels:
                if geo_label in geolabel_to_municipality_taxid:
                    mun_tax_id = geolabel_to_municipality_taxid[geo_label]

                    if mun_tax_id in municipality_taxonomy_mappings:
                        municipality_data = municipality_taxonomy_mappings[mun_tax_id]

                        address_item = {
                            "municipality_concept_id": municipality_data['mun_id'],
                            "municipality": municipality_data['mun_preferred_label'],
                            "region_concept_id": municipality_data['region_id'],
                            "region": municipality_data['region_preferred_label'],
                            "country_concept_id": municipality_data['country_id'],
                            "country": municipality_data['country_preferred_label']
                        }

                        workplace_addresses.append(address_item)

                elif geo_label in country_label_to_data_mappings:
                    country_data = country_label_to_data_mappings[geo_label]
                    address_item = {
                        "municipality_concept_id": "",
                        "municipality": "",
                        "region_concept_id": "",
                        "region": "",
                        "country_concept_id": country_data['id'],
                        "country": country_data['preferred_label']
                    }
                    workplace_addresses.append(address_item)

            ad['workplace_addresses'] = remove_duplicates(workplace_addresses)
        except:
            e = sys.exc_info()[0]
            print_error(e)

    # write_ads_to_file(ads)
    print_ads(ads)
