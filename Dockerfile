FROM docker.io/library/ubuntu:22.04 AS base

WORKDIR /app

COPY requirements.txt requirements.txt
COPY . /app

RUN apt-get -y update &&\
    apt-get -y install python3 python3-pip &&\
    python3 -m pip install --upgrade pip && pip install -r requirements.txt


###############################################################################
FROM base AS test

COPY tests/testdata/ /testdata/

RUN python3 main.py < /testdata/input.10 > /tmp/output.10 \
    && diff /testdata/expected_output.10 /tmp/output.10 \
    && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

CMD ["python3","main.py"]
